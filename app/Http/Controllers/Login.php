<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class Login extends Controller
{
    function ViewLogin()
    {
        return view('login.index');
    }

    function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {

        //  dd( Auth::user()->role);

            $request->session()->regenerate();

            return redirect()->intended('dashboard');
        }

        return back();
    }
    function registerView()
    {
        return view('login.register');
    }
    function storeRegister(Request $request)
    {
        $status = User::where('email', $request->email)->count();
        if ($status == 0) {
            $user = User::create([
                'name' => 'test',
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            return redirect('/login');
        }
        return back();
    }
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
