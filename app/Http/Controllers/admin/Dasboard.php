<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class Dasboard extends Controller
{
    
    function __construct()
    {
        $this->middleware('checkrules:admin');
    }

    function index()
    {
        return view('admin.dasboard');
    }
}
