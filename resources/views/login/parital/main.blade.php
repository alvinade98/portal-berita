<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('judul_halaman')</title>
</head>
<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="/assets/css/login.css">
<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">

        @yield('konten')


<script src="/assets/bootstrap/js/bootstrap.js"></script>



