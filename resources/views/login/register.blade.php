@extends('login.parital.main')
@section('judul_halaman', 'Register')
@section('konten')
<body>
   <div class="container mt-4">
    <div class="row">
        <div class="col-2 col-md-3"></div>
        <div class="col-8  col-sm-6 mt-4  card">
            <div class="text-judul mt-4 text-center">Register</div>
            <div class="mb-4 mt-2 ">
                <form action="/register" method="post">
                   @csrf
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Email</label>
                      <input type="email" class="form-control" placeholder="Masukan Email Anda" id="email" name="email" required aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Password</label>
                      <input type="password" class="form-control" placeholder="Masukan Password Anda" id="password" required name="password">
                    </div>
                    <div class="mb-3 form-check">
                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    </div>
                    <div class="d-flex justify-content-center">
                        <div>
                            <button type="submit" class="btn btn-primary text-center">Register</button>
                        </div>
                    </div>
                  </form>
            </div>
        </div>
        <div class="col-2 col-sm-3"></div>
    </div>
   </div>
</body>
</html>
@endsection