<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Login;
use App\Http\Controllers\admin\Dasboard;
use App\Http\Controllers\user\DasboardUser;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [Login::class, 'ViewLogin'])->name('login')->middleware(['guest','revalidate']);
Route::post('/login', [Login::class, 'authenticate']);

Route::get('/register', [Login::class, 'registerView'])->middleware('guest','revalidate');
Route::post('/register', [Login::class, 'storeRegister']);

Route::post('/logout', [Login::class, 'logout']);

Route::get('/dashboard', [Dasboard::class, 'index'])->middleware(['checkrules:admin','revalidate']);

Route::get('/user/dashboard', [DasboardUser::class, 'index'])->middleware(['checkrules:user','revalidate']);

